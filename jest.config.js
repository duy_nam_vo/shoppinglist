// For a detailed explanation regarding each configuration property, visit:
// https://jestjs.io/docs/en/configuration.html

module.exports = {
  clearMocks: true,
  coverageDirectory: "coverage",
  testEnvironment: 'jsdom',
  verbose: true,
  testURL: 'http://localhost/',
  transform: {
    '^.+\\.(ts|tsx)?$': 'ts-jest',
    "^.+\\.(js|jsx)$": "babel-jest",
  },
  setupFiles: ["./src/setupTests.ts"],
  transformIgnorePatterns: ['<rootDir>/node_modules/'],
};
