import React from "react";
import ReactDOM from "react-dom";
import Header from './components/Header';
import Footer from './components/Footer';
import './scss/app.scss';
import { DndProvider } from 'react-dnd';
import Backend from 'react-dnd-html5-backend';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import Home from "./components/Home/index";
import About from "./components/About";
import Users from "./components/Users";
import { Provider } from "./context/commonContext";
import dotenv from 'dotenv';
dotenv.config();

const App = (): JSX.Element => {
  return (
    <Router>
      <Provider>
        <DndProvider backend={Backend}>
            <div>
              <Header />
              <div className="mb4">
                <Switch>
                  <Route path="/about">
                    <About />
                  </Route>
                  <Route path="/users">
                    <Users />
                  </Route>
                  <Route path="/">
                    <Home />
                  </Route>
                </Switch>
              </div>
              <Footer />
            </div>
        </DndProvider>
      </Provider>
    </Router>
  )
}

let element = document.getElementById("app");

ReactDOM.render(<App />, element);