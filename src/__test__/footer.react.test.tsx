import React from 'react';
import Footer from '../components/Footer';
import renderer from 'react-test-renderer';

describe("Footer component", () => {
  test("Matches the footer snapshot", () => {
    const footer = renderer.create(
      <Footer />);
    expect(footer.toJSON()).toMatchSnapshot();
  });
});