import React from 'react';
import Home from '../components/Home/index';
import { mount, shallow } from 'enzyme';
import { DndProvider } from 'react-dnd';
import Backend from 'react-dnd-html5-backend';
import { Provider } from "../context/commonContext";


describe('Shopping list component', () => {
  const wrapper = mount(<DndProvider backend={Backend}>
    <Provider>
      <Home />
    </Provider>
  </DndProvider>);
  test('It render an empty shopping list', () => {
    expect(wrapper.find("div[data-type='item']")).toHaveLength(1);
    expect(wrapper.find("input[name='name']").text()).toBe("");
    expect(wrapper.find("input[name='quantity']").text()).toBe("");
  });

  test('It should render a button with the text Add', () => {
    const button = wrapper.find("button[data-function='add']");
    expect(button.text()).toBe('Add');
  });

  test('It should render a single name input', () => {
    const input = wrapper.find("div[data-type='item']");
    expect(input).toHaveLength(1);
  });

  test('It should add a row when clicking on Add', () => {
    const wrapper = mount(<DndProvider backend={Backend}>
      <Provider>
        <Home />
      </Provider>
    </DndProvider>);
    const button = wrapper.find("button[data-function='add']");
    button.simulate('click');
    const input = wrapper.find("div[data-type='item']");
    expect(input).toHaveLength(2);
  });

  test('It should delete the row when clicking on delete icon', () => {
    const wrapper = mount(<DndProvider backend={Backend}>
      <Provider>
        <Home />
      </Provider>
    </DndProvider>);
    const button = wrapper.find("button[data-function='add']");
    button.simulate('click');
    button.simulate('click');
    button.simulate('click');
    const deleteIcon = wrapper.find("i[data-function]").at(1);
    deleteIcon.simulate('click');
    const input = wrapper.find("div[data-type='item']");
    expect(input).toHaveLength(3);
  });

  test('It should delete the correct row', () => {
    const wrapper = mount(<DndProvider backend={Backend}>
      <Provider>
        <Home />
      </Provider>
    </DndProvider>);
    const button = wrapper.find("button[data-function='add']");
    button.simulate('click');
    button.simulate('click');
    button.simulate('click');
    const deleteIcon = wrapper.find("i[data-function]").at(1);
    const selectedUuid = wrapper.find("div[data-type='item']").at(1).prop("data-uuid");
    deleteIcon.simulate('click');
    expect(wrapper.find(`div[data-uuid='${selectedUuid}']`)).toHaveLength(0);
  });

  test('It should update the name field', () => {
    const inputName = wrapper.find("input[name='name']").at(0);
    inputName.simulate("change", { target: { value: 'test' } });
    expect(wrapper.find("input[name='name']").at(0).render().val()).toBe('test');
  });
  test('It should update the quantity field', () => {
    const inputName = wrapper.find("input[name='quantity']").at(0);
    inputName.simulate("change", { target: { value: 10 } });
    expect(wrapper.find("input[name='quantity']").at(0).render().val()).toBe("10");
  });

  test('It should not register string for quantity field', () => {
    const wrapper = mount(<DndProvider backend={Backend}>
      <Provider>
        <Home />
      </Provider>
    </DndProvider>);
    const inputName = wrapper.find("input[name='quantity']").at(0);
    inputName.simulate("change", { target: { value: "test" } });
    expect(wrapper.find("input[name='quantity']").at(0).render().val()).toBe("0");
  });

  test('It should not delete row when only one left but reset value', () => {
    const wrapper = mount(<DndProvider backend={Backend}>
      <Provider>
        <Home />
      </Provider>
    </DndProvider>);
    const inputName = wrapper.find("input[name='name']").at(0);
    inputName.simulate("change", { target: { value: 'test' } });
    const inputQty = wrapper.find("input[name='quantity']").at(0);
    inputQty.simulate("change", { target: { value: 10 } });
    const deleteIcon = wrapper.find("i[data-function]").at(0);
    deleteIcon.simulate("click");
    expect(wrapper.find("div[data-type='item']")).toHaveLength(1);
    expect(wrapper.find("input[name='name']").at(0).render().val()).toBe("");
    expect(wrapper.find("input[name='quantity']").at(0).render().val()).toBe("0");
  });

  test('It should display an input title', () => {
    const inputTitle = wrapper.find("input[name='title']").at(0);
    expect(inputTitle).toHaveLength(1);
  });

  test('It should change the input title', () => {
    const inputTitle = wrapper.find("input[name='title']").at(0);
    inputTitle.simulate('change', { target: { value: 'new title' } });
    expect(wrapper.find("input[name='title']").at(0).render().val()).toBe("new title");
  });
});