export const DragType ={
  ITEM:'item',
  CARD:'card',
  POST_IT:'postIt',
}

export const DropType = {
  MOVE:'move',
  TRANSFER:'transfer',
}

export const TargetType = {
  BOARD:'board',
}