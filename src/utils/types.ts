export interface ShoppingItem {
  id: string;
  name?: string;
  quantity?: number;
}
export interface ShoppingList {
  id: string;
  title: string;
  items: ShoppingItem[];
}