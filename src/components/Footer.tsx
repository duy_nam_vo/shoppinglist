import React from 'react';

const Footer = (): JSX.Element => <footer className="py2">
	<div className="flex justify-between col-11 mx-auto">
		<span>MAde by Duy Nam</span>
		<span>Connect us</span>
	</div>
</footer>

export default Footer;