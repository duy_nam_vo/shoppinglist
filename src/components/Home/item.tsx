import React, { useRef, useEffect } from 'react';
import { ShoppingItem } from '../../utils/types';
import { useDrag, useDrop, DropTargetMonitor, DragSourceMonitor } from 'react-dnd';
import { XYCoord } from 'dnd-core';
import { DragType, DropType } from '../../utils/static';
import { useDispatch } from '../../context/commonContext';



interface Props {
	item: ShoppingItem;
	setItem(item: ShoppingItem): void;
	deleteItem(itemId: string): void;
	moveItem: (dragIndex: number, hoverIndex: number) => void;
	index: number;
	parentId: string;
}

interface DragItem {
	index: number
	id: string
	type: string
}

interface DropResult {
	dropEffect: keyof typeof DropType;
	name: string;
	type: string;
	id: string;
}

const Item = ({ item, setItem, deleteItem, moveItem, index, parentId }: Props): JSX.Element => {
	const dispatch = useDispatch();
	const changeItem = (inputName: keyof ShoppingItem, value: number | string) => {
		const newItem = Object.assign({}, item, { [inputName]: value });
		setItem(newItem);
	}
	const { id, name, quantity } = item;

	const ref = useRef<HTMLDivElement>(null)
	const dropHover = (item: DragItem, monitor: DropTargetMonitor) => {
		if (!ref.current) return;
		const dragIndex = item.index
		const hoverIndex = index
		if (dragIndex === hoverIndex) return;
		const hoverBoundingRect = ref.current!.getBoundingClientRect()
		const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2
		const clientOffset = monitor.getClientOffset()
		const hoverClientY = (clientOffset as XYCoord).y - hoverBoundingRect.top
		if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) return;
		if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) return;
		moveItem(dragIndex, hoverIndex)
		item.index = hoverIndex
	};

	const dragEnd = (item: DragItem | void, monitor: DragSourceMonitor) => {
		const dropResult: DropResult = monitor.getDropResult()
		if (dropResult && dropResult.type === 'board') {
			if (parentId === dropResult.id) return;
			dispatch({
				type: 'transferItemToList',
				fromId: parentId,
				itemId: item && item.id || "",
				toId: dropResult.id
			})
		}
	}

	const [{ canDrop, isOver, difference }, drop] = useDrop({
		accept: DragType.ITEM,
		hover: dropHover,
		collect: (monitor: DropTargetMonitor) => ({
			isOver: monitor.isOver({ shallow: true }),
			canDrop: monitor.canDrop(),
			difference: monitor.getDifferenceFromInitialOffset(),
		}),
	})

	const [{ isDragging }, drag] = useDrag({
		item: { type: DragType.ITEM, id, index },
		end: dragEnd,
		collect: (monitor: DragSourceMonitor) => ({
			isDragging: monitor.isDragging(),
		}),
	})
	const opacity = isDragging ? 0 : 1
	drag(drop(ref))

	return (
		<div ref={ref} style={{ opacity }} className="columns mt1">
			{isOver && <div style={{ outline: 'red solid' }} />}
			<div data-type="item" className="column is-half" data-uuid={id}>
				<input
					value={name || ""}
					className="input" type="text"
					placeholder="Enter item type"
					name="name"
					onChange={(ev) => changeItem("name", ev.target.value)}
				/>
			</div>
			<div className="column is-one-fifth flex">
				<input
					value={quantity || 0}
					className="input"
					type="number"
					placeholder="Item quantity"
					name="quantity"
					onChange={(ev => {
						if (isNaN(Number(ev.target.value))) return;
						changeItem("quantity", ev.target.value);
					})}
				/>
			</div>
			<div className="column is-one-fifth">
				<span className="icon self-center delete-cursor ml1" onClick={() => deleteItem(item.id)}>
					<i data-function="delete" className="far fa-times-circle" style={{ color: 'red' }}></i>
				</span>
			</div>
		</div>)
}
export default Item;