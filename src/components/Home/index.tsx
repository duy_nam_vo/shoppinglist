import React, { useCallback } from "react";
import { ShoppingItem, ShoppingList } from "../../utils/types";
import Item from './item';
import { useDispatch, useGlobalState } from '../../context/commonContext';
import DashBoard from '../Dashboard';

const Home = () => {
	const lists = useGlobalState("lists");
	const activeListId = useGlobalState("activeListId");
	const dispatch = useDispatch();
	const { title, id, items } = lists.find(el => el.id === activeListId) || {} as ShoppingList;

	const addItems = () => {
		dispatch({ type: 'addItemToList', id });
	}
	const setItem = (item: ShoppingItem): void => {
		dispatch({ type: 'updateItemInList', item, id });
	}
	const deleteItem = (itemId: string): void => {
		if (items.length === 1) {
			dispatch({ type: 'setListToDefault', id });
			return;
		}
		dispatch({ type: 'removeItemFromList', id, itemId });
	}
	const moveItem = useCallback(
		(dragIndex: number, hoverIndex: number) => {
			dispatch({ type: 'moveItem', id, dragIndex, hoverIndex });
		},
		[items],
	)

	return (
		<div className="flex">
			<div className="pl2 col-9 mx-auto flex flex-column shoppingList">
				<input
					value={title}
					className="inputTitle mt1 col-5 p1" type="text"
					placeholder="Enter title"
					name="title"
					onChange={(ev) => dispatch({
						type: 'setTitle',
						id,
						title: ev.target.value
					})}
				/>
				<div>
					{items.map((item, i) => <Item
						deleteItem={deleteItem}
						setItem={setItem}
						index={i}
						key={item.id}
						item={item}
						moveItem={moveItem}
						parentId={id}
					/>)}
				</div>
				<div className="flex">
					<button
						data-function="add"
						className="button mt1 is-primary"
						onClick={addItems}
					>
						Add
					</button>
				</div>
			</div>
			<div className="col-3">
				<DashBoard />
			</div>
		</div>
	)
};
export default Home;