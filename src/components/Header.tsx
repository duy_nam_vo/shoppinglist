import React from 'react'
import { Link } from 'react-router-dom'
const Header = (): JSX.Element => (
	<header>
		<nav className="navbar" role="navigation" aria-label="main navigation">
			<div className="navbar-brand">
				<Link className="navbar-item" to="/">Home</Link>
				<Link className="navbar-item" to="/about">Another section</Link>
				<Link className="navbar-item" to="/users">User</Link>
			</div>
		</nav>
	</header>
)

export default Header;