import React from 'react';
import { useDispatch, useGlobalState } from '../../context/commonContext';
import Board from './board';

const Dashboard = () => {
  const lists = useGlobalState("lists");
  const dispatch = useDispatch();
  return (
    <div className="dashboard flex flex-column">
      <div className="flex flex-wrap">
        {lists.map(list => <Board key={list.id} shoppingList={list} />)}
      </div>
      <div>
        <button
          data-function="add-list"
          className="button mt1 is-primary"
          onClick={() => dispatch({ type: 'addEmptyList' })}
        >
          Add list
					</button>
      </div>
    </div>
  );
}

export default Dashboard;