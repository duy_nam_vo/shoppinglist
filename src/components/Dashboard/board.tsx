import React, { useRef } from 'react';
import { useDrop, DropTargetMonitor } from 'react-dnd';
import { useDispatch, useGlobalState } from '../../context/commonContext';
import { DragType, DropType, TargetType } from '../../utils/static';
import { ShoppingList } from '../../utils/types';
import clsx from 'clsx';

interface Props {
  shoppingList: ShoppingList
}

const Board = ({ shoppingList }: Props) => {
  const { id, title } = shoppingList;
  const ref = useRef<HTMLDivElement>(null);
  const [{ canDrop, isOver }, drop] = useDrop({
    accept: DragType.ITEM,
    drop: () => ({
      name: `${title}`,
      type: TargetType.BOARD,
      id,
      dropEffect: DropType.TRANSFER,
    }),
    collect: (monitor: DropTargetMonitor) => ({
      canDrop: monitor.canDrop(),
      isOver: monitor.isOver({ shallow: false }),
    })
  });
  drop(ref);
  const activeListId = useGlobalState("activeListId");
  const dispatch = useDispatch();
  return (
    <div
      ref={ref}
      onClick={() => { dispatch({ type: 'setActiveList', id }) }}
      className={clsx("flex flex-column ml1 p1 board",
        activeListId === id && "selected",
        canDrop && 'can-drop',
        isOver && 'is-over',
      )}
    >
      <div className="listIcon" />
      <span>{title}</span>
    </div>
  )
}

export default Board;