import { ShoppingList, ShoppingItem } from '../utils/types';
import produce from 'immer';
import { v4 as uuidv4 } from 'uuid';
const uuid = uuidv4();
export const initialState = {
  lists: [
    {
      id: uuid,
      title: ''
      , items: [
        {
          id: uuidv4(),
          name: '',
          quantity: 0,
        },

      ]
    },

  ] as ShoppingList[],
  activeListId: uuid,
};

export type State = typeof initialState;

export type Action =
  | { type: 'addList', list: ShoppingList }
  | { type: 'addEmptyList' }
  | { type: 'removeList', id: string }
  | { type: 'addItemToList', id: string }
  | { type: 'removeItemFromList', itemId: string, id: string }
  | { type: 'updateItemInList', item: ShoppingItem, id: string }
  | { type: 'setListToDefault', id: string }
  | { type: 'moveItem', id: string, dragIndex: number, hoverIndex: number }
  | { type: 'setTitle', id: string, title: string }
  | { type: 'setActiveList', id: string }
  | { type: 'transferItemToList', fromId: string, toId: string, itemId: string }

export const reducer = (state: State, action: Action) => {
  switch (action.type) {
    case 'addList': return produce(state, draft => {
      draft.lists[draft.lists.length] = action.list;
    });

    case 'addEmptyList': return produce(state, draft => {
      draft.lists[draft.lists.length] = { id: uuidv4(), title: 'Untitled', items: [{ id: uuidv4(), name: '', quantity: 0 }] }
    })

    case 'removeList': return produce(state, draft => {
      draft.lists = draft.lists.filter(el => el.id !== action.id);
    });

    case 'addItemToList':
      return produce(state, draft => {
        const newItem: ShoppingItem = { id: uuidv4(), name: '', quantity: 0 };
        const list = draft.lists.find(el => el.id === action.id);
        if (!list) return;
        list.items = [...list?.items, newItem];
      });

    case 'updateItemInList':
      return produce(state, draft => {
        const { id, item } = action;
        const list = draft.lists.find(el => el.id === id);
        if (!list) return;
        const foundIndex = list.items.findIndex(el => el.id === item.id);
        list.items[foundIndex] = item;
      });

    case 'setListToDefault':
      return produce(state, draft => {
        const { id } = action;
        const list = draft.lists.find(el => el.id === id);
        if (!list) return;
        list.items = [{ id: uuidv4(), name: '', quantity: 0 }];
      });

    case 'removeItemFromList':
      return produce(state, draft => {
        const { id, itemId } = action;
        const list = draft.lists.find(el => el.id === id);
        if (!list) return;
        list.items = list.items.filter(el => el.id !== itemId);
      });

    case 'moveItem':
      return produce(state, draft => {
        const { id, dragIndex, hoverIndex } = action;
        const list = draft.lists.find(el => el.id === id);
        if (!list) return;
        const { items } = list;
        const dragItem = items[dragIndex];
        items.splice(dragIndex, 1);
        items.splice(hoverIndex, 0, dragItem);
      });

    case 'setTitle':
      return produce(state, draft => {
        const { id, title } = action;
        const list = draft.lists.find(el => el.id === id);
        if (!list) return;
        list.title = title;
      });

    case 'setActiveList':
      return produce(state, draft => {
        const { id } = action;
        const found = draft.lists.find(el => el.id === id);
        if (found && draft.activeListId !== id) draft.activeListId = id;
      });
      
    case 'transferItemToList':
      return produce(state, draft => {
        const { lists } = draft;
        const { fromId, toId, itemId } = action;
        const item = lists.find(el => el.id === fromId)?.items.find(item => item.id === itemId);
        const fromList = lists.find(list => list.id === fromId);
        if (!item || !fromList) return state;
        lists.find(list => list.id === toId)?.items.push(item);
        fromList.items = fromList.items?.filter(item => item.id !== itemId);
      });
    // default: return state;
  }
};