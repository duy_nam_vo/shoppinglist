import { reducer, initialState } from './common';
import { ShoppingList, ShoppingItem } from '../utils/types';
import { v4 as uuidv4 } from 'uuid';

describe('Reducer test', () => {
  const { lists } = initialState;
  const [firstList] = lists;

  const list: ShoppingList = {
    id: uuidv4(),
    title: 'Written for test',
    items: [{ id: uuidv4(), name: 'toilet paper', quantity: 9 }],
  }

  test('It should add a list', () => {
    const newLists = reducer(initialState, { type: 'addList', list });
    const [lastList] = newLists.lists.slice(-1);
    expect(newLists.lists.length).toBe(lists.length + 1);
    expect(JSON.stringify(newLists.lists)).not.toBe(JSON.stringify(initialState.lists));
    expect(JSON.stringify(lastList.title)).toBe("\"Written for test\"");
    expect(JSON.stringify(list)).toBe(JSON.stringify(lastList));
  });

  test('It should remove the list', () => {
    const newLists = reducer(initialState, { type: 'addList', list });
    const listRemoved = reducer(newLists, { type: 'removeList', id: list.id });
    expect(listRemoved.lists.length).toBe(newLists.lists.length - 1);
    expect(listRemoved.lists.find(el => el.id === list.id)).toBe(undefined);
  });

  test('It should add item in a list', () => {
    const newLists = reducer(initialState, { type: 'addList', list });
    expect(newLists.lists.find(el => el.id === list.id)?.items.length).toBe(1);
    const addItemLists = reducer(newLists, { type: 'addItemToList', id: list.id });
    expect(addItemLists.lists.find(el => el.id === list.id)?.items.length).toBe(2);
  });

  test('It should add an empty list', () => {
    const newLists = reducer(initialState, { type: 'addEmptyList' });
    expect(lists.length + 1).toBe(newLists.lists.length);
  });

  test('It should update an item in existing list', () => {
    const newLists = reducer(initialState, { type: 'addEmptyList' });
    const [lastList] = newLists.lists.slice(-1);
    const { id, items } = lastList;
    const [first] = items;
    const changedList = reducer(newLists, {
      type: 'updateItemInList',
      id,
      item: {
        id: first.id,
        name: 'test',
        quantity: 1
      }
    });
    const [lastChangedList] = changedList.lists.slice(-1);
    const { items: changedItems } = lastChangedList;

    expect(JSON.stringify(newLists)).not.toBe(JSON.stringify(changedList));
    expect(changedItems.find(el => el.id === first.id)?.name).toBe('test');
    expect(changedItems.find(el => el.id === first.id)?.quantity).toBe(1);
  });

  test('It should remove an item from a list', () => {
    const id = uuidv4();
    const itemId = uuidv4();
    const initial: ShoppingList[] = [
      {
        id,
        title: 'test list 1',
        items: [
          { id: uuidv4(), name: 'test', quantity: 1 },
          { id: itemId, name: 'test2', quantity: 2 },
        ]
      }
    ];
    const newLists = reducer(
      { lists: initial, activeListId: '' },
      { type: 'removeItemFromList', id, itemId });
    expect(newLists.lists[0].items.length).toBe(1);
  });

  test('It should set the list to default', () => {
    const id = uuidv4();
    const initial: ShoppingList[] = [
      {
        id,
        title: 'test list 1',
        items: [
          { id: uuidv4(), name: 'test', quantity: 1 },
          { id: uuidv4(), name: 'test2', quantity: 2 },
          { id: uuidv4(), name: 'test3', quantity: 2 },
          { id: uuidv4(), name: 'test4', quantity: 2 },
        ]
      }
    ];
    const newList = reducer(
      {
        lists: initial,
        activeListId: ''
      },
      { type: 'setListToDefault', id });
    const { lists } = newList;
    const [shoppingList] = lists;
    const { items } = shoppingList;
    expect(items.length).toBe(1);
    expect(items[0].name).toBe("");
    expect(items[0].quantity).toBe(0);
  });

  test('It should move the item accordingly', () => {
    const id = uuidv4();
    const firstItemId = uuidv4();
    const lastItemId = uuidv4();
    const lists: ShoppingList[] = [
      {
        id,
        title: 'test list 1',
        items: [
          { id: firstItemId, name: 'test', quantity: 1 },
          { id: uuidv4(), name: 'test2', quantity: 2 },
          { id: uuidv4(), name: 'test3', quantity: 2 },
          { id: lastItemId, name: 'test4', quantity: 2 },
        ]
      }
    ];

    let testLists = reducer({ lists, activeListId: '' },
      {
        type: 'moveItem',
        id,
        dragIndex: 0,
        hoverIndex: 2
      });
    expect(testLists.lists[0].items.findIndex(el => el.id === firstItemId)).toBe(2);

    testLists = reducer({ lists, activeListId: '' },
      {
        type: 'moveItem',
        id,
        dragIndex: 3,
        hoverIndex: 0
      });
    expect(testLists.lists[0].items.findIndex(el => el.id === lastItemId)).toBe(0);
  });
  test('It should set the title for the list', () => {
    const id = uuidv4();
    const lists: ShoppingList[] = [
      {
        id,
        title: 'test list 1',
        items: [
          { id: uuidv4(), name: 'test', quantity: 1 },
          { id: uuidv4(), name: 'test2', quantity: 2 },
          { id: uuidv4(), name: 'test3', quantity: 2 },
          { id: uuidv4(), name: 'test4', quantity: 2 },
        ]
      }
    ];
    const testLists = reducer({ lists, activeListId: '' }, { type: 'setTitle', id, title: 'new_title' });
    expect(testLists.lists[0].title).toBe('new_title');
  });
  test('It should set to a selected active list', () => {
    const id = uuidv4();
    const id2 = uuidv4();
    const lists: ShoppingList[] = [
      {
        id,
        title: 'test list 1',
        items: []
      },
      {
        id: id2,
        title: 'test list 1',
        items: []
      },
    ];
    const testList = reducer(
      {
        lists,
        activeListId: id
      },
      {
        type: 'setActiveList',
        id: id2
      });
    expect(testList.activeListId).toBe(id2);
    expect(reducer({ lists, activeListId: id }, { type: 'setActiveList', id: 'tttt' }).activeListId).toBe(id);
  });
  test('It transfer an item to another list', () => {
    const id = uuidv4();
    const id2 = uuidv4();
    const itemId = uuidv4();
    const lists: ShoppingList[] = [
      {
        id,
        title: 'test list 1',
        items: [{
          id: itemId,
          name: '',
          quantity: 0,
        }]
      },
      {
        id: id2,
        title: 'test list 1',
        items: []
      },
    ];
    const testList = reducer({ lists, activeListId: '' }, 
    { type: 'transferItemToList', fromId: id, toId: id2, itemId });
    const [firstList,secondList] = testList.lists;
    expect(firstList.items.length).toBe(0);
    expect(secondList.items.length).toBe(1);
  });

});